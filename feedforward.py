import sys
import cPickle
import numpy
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

class HiddenLayer(object):
    def __init__(self, input=None, W=None, b=None,
                 activation=None):
        """
        Typical hidden layer of a MLP: units are fully-connected and have
        sigmoidal activation function. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).

        NOTE : The nonlinearity used here is sigmoid

        Hidden unit activation is given by: tanh(dot(input,W) + b)

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type W: theano.tensor.dmatrix
        :param W: Weight matrix of the layer

        :type b: theano.tensor.dvector
        :param b: biases of the layer

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden
                           layer
        """

        # __init__ never gets run for unpickling so this function is just
        # legacy code
        print 'Initialize'
        self.input = input

        self.W = W
        self.b = b


    def __setstate__(self, state):
        W, b, activation = state
        self.W = W
        self.b = b
        self.activation = activation
        
                
        
    def setinput(self, input1):
        self.input = input1
        lin_output = T.dot(self.input, self.W) + self.b
        self.output = (lin_output if self.activation is None
                       else self.activation(lin_output))



class MLP(object):
    
    def __init__(self, rng, input, wts_file):
        
        self.layers = []
        # Open the wts file to get the number of layers
        s = file(wts_file, 'rb')

        while True:
            try:
                self.layers.append(cPickle.load(s))
            except (EOFError):
                break
        s.close()

        self.num_layers = len(self.layers)

        self.layers[0].setinput(input)

        for i in xrange(1, len(self.layers)):
            self.layers[i].setinput(self.layers[i-1].output)



def run_mlp(wtsfile, input_file, output_file):

    ip_data = T.matrix('ip_data') # Input data is a matrix
    #op_data = T.matrix('op_data') # Output data is The Matrix(c)


    rng = numpy.random.RandomState(1234) #Seed so that experiments are replicable
    
    our_mlp = MLP(rng=rng, input=ip_data, wts_file=wtsfile)

    ipdata = numpy.loadtxt(input_file,dtype="float32")

    ipdata_shared = theano.shared(numpy.asarray(ipdata,
                                                dtype=theano.config.floatX))



    # Let's compile a Theano function that can run our MLP on our frames
    run_model = theano.function(outputs = our_mlp.layers[our_mlp.num_layers-1].output,
                                inputs = [ip_data])
    
    op_data = run_model(ipdata)

    numpy.savetxt(output_file, op_data, delimiter='\t', newline='\n')
    



if __name__ == '__main__':
    if len(sys.argv) <= 2:
        print "Usage: python feedforward.py WTS.file INPUTDATA.ascii OUTPUT.ascii"
        sys.exit(0)
    else:
        run_mlp(wtsfile=sys.argv[1], input_file=sys.argv[2], 
                output_file=sys.argv[3])
                           

