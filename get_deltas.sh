#!/bin/bash

PROMPTFILE="$1"

if [ "$PROMPTFILE" = "" ]
then
    PROMPTFILE=etc/txt.done.data
fi


SPTK=/home/pmuthuku/toolkits/SPTK-3.7/binaries/
CODE_DIR=/home/pmuthuku/code/Theano_exps/

CG_TMP=jnk_$$
ORDER=50

cat $PROMPTFILE |
awk '{print $2}' | 
while read i
do

    fname=$i
    echo "Extracting deltas for $fname"

    perl /home/pmuthuku/code/error_metrics/mean_sd.pl encd/$fname.encd 1 50 > $CG_TMP.meansd

    #Dithering data
    #perl $CODE_DIR/dither_encoding.pl encd/$fname.encd $CG_TMP.meansd > $CG_TMP.dithered

    #$SPTK/bin/x2x +ad < $CG_TMP.dithered > $CG_TMP.encd
    $SPTK/bin/x2x +ad < encd/$fname.encd > $CG_TMP.encd

    $FESTVOXDIR/src/vc/src/mlpg/delta -nmsg -jnt -dynwinf $FESTVOXDIR/src/vc/src/win/dyn.win -dim $ORDER $CG_TMP.encd $CG_TMP.encd_delt

    perl $FESTVOXDIR/src/clustergen/d2a.pl < $CG_TMP.encd_delt |
    awk '{printf("%s ",$1); if ((NR%(2*'$ORDER')) == 0)  printf("\n")}' > encd/$fname.encd_delt

done

rm -f $CG_TMP.*