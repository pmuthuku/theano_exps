"""
This code reads in a pickled SdA model file and then uses that
to initialize and train a Multilayer Perceptron.

"""

import cPickle
import sys
import numpy
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

class dA(object):
    def __init__(self, rng, input, n_in, n_out, W=None, b=None,
                 activation=T.nnet.sigmoid):
        """
        Typical hidden layer of a MLP: units are fully-connected and have
        sigmoidal activation function. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).

        NOTE : The nonlinearity used here is tanh

        Hidden unit activation is given by: tanh(dot(input,W) + b)

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden
                           layer
        """
        
        pass

    def __setstate__(self, state):
        W, b, b_prime, activation = state
        self.W = W
        self.b = b
        self.b_prime = b_prime
        self.activation = activation


class HiddenLayer(object):
    def __init__(self, rng, input, W, b,
                 activation=T.nnet.sigmoid):
        """
        Typical hidden layer of a MLP: units are fully-connected and have
        sigmoidal activation function. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).

        NOTE : The nonlinearity used here is sigmoid

        Hidden unit activation is given by: tanh(dot(input,W) + b)

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type W: theano.tensor.dmatrix
        :param W: Weight matrix of the layer

        :type b: theano.tensor.dvector
        :param b: biases of the layer

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden
                           layer
        """
            
        self.input = input

        # self.W = theano.shared(value=W.get_value(),
        #                        name='W', borrow=True)

        # initial_b = theano.shared(value=b.get_value(),
        #                           name = 'b', borrow=True)
        
        # self.b = initial_b

        self.W = theano.shared(value=W, name='W', borrow=True)
        self.b = theano.shared(value=b, name='b', borrow=True)

        self.activation=activation

        lin_output = T.dot(self.input, self.W) + self.b
        self.output = (lin_output if activation is None
                       else activation(lin_output))

        # parameters of the model
        self.params = [self.W, self.b]

    def __getstate__(self):
        return (self.W, self.b, self.activation)


class MLP(object):
    
    def __init__(self, rng, da_layers, inputdata=None):
        
        self.layers = []
        self.params = []
        
        # This is actually the number of 'small' MLPs 
        self.num_layers = len(da_layers)*2

        # If no input is given, generate a variable representing
        # the input
        if inputdata == None:
            self.input = T.dmatrix(name='input')
        else:
            self.input = inputdata
        
        # First layer
        self.layers.append(HiddenLayer(rng=rng, input=self.input, 
                                       W=da_layers[0].W.get_value(),
                                       b=da_layers[0].b.get_value(),
                                       activation=da_layers[0].activation ))
        self.params.extend(self.layers[0].params)

        # First half of the network
        for i in xrange(1, len(da_layers)):
            self.layers.append(HiddenLayer(rng=rng, 
                                           input=self.layers[i-1].output,
                                           W=da_layers[i].W.get_value(), 
                                           b=da_layers[i].b.get_value(), 
                                           activation=da_layers[i].activation ))
            self.params.extend(self.layers[i].params)

        # Second half of network
        for i,j in zip( xrange(len(da_layers),self.num_layers), reversed(xrange(len(da_layers))) ):
            self.layers.append(HiddenLayer(rng=rng,
                                           input=self.layers[i-1].output,
                                           W=da_layers[j].W.get_value().transpose(), 
                                           b=da_layers[j].b_prime.get_value(),
                                           activation=da_layers[j].activation ))
            self.params.extend(self.layers[i].params)
            
    def rmse_error(self):
        """ Computes Root Mean Squared Error between the input and
        the output of the network """
        # This function really needs double or triple checking
        
        y1 = self.layers[self.num_layers-1].output
        
        L = T.mean(T.square(self.input - y1 ))
        cost = T.sqrt(T.mean(L)) # Probably don't need the second mean
        return cost

                          


def train_mlp(sda, dataset, enc_wts, dec_wts, learning_rate=0.1, 
              n_epochs=100, batch_size=100):
    
    da_layers=[]
    f = file(sda,'rb')
    
    index = T.lscalar() # index to a minibatch
    x = T.matrix('x')   # This is the input data variable

    # Ugliest piece of python code I have ever seen:    
    while True:
        try:
            da_layers.append(cPickle.load(f))
        except (EOFError):
            break
    f.close()

    # numpy random generator
    numpy_rng = numpy.random.RandomState(89677)
    our_mlp=MLP(rng=numpy_rng, inputdata=x, da_layers=da_layers)

    cost = our_mlp.rmse_error()
    
    # Compute the gradient of cost with respect to the parameters
    # and store the gradients in gparams
    gparams = []
    for param in our_mlp.params:
        gparam = T.grad(cost, param)
        gparams.append(gparam) 
        #Note ^ that one is gparams and the other is params

    updates = []
    for param, gparam in zip(our_mlp.params, gparams):
        updates.append((param, param - learning_rate * gparam))


    # Load dataset     Think about why this is 64 \|/ ???
    all_data = numpy.loadtxt(dataset,dtype="float32")
    splitting_point = int(all_data.shape[0]*0.9)
    train_set = all_data[splitting_point:,:]
    valid_set = all_data[splitting_point+1:,:]

    # Doing this for reasons I don't fully understand ...yet 
    train_shared_set = theano.shared(train_set, borrow=True)
    valid_shared_set = theano.shared(valid_set, borrow=True)

    n_train_batches = train_shared_set.get_value(borrow=True).shape[0]/batch_size
    n_valid_batches = valid_shared_set.get_value(borrow=True).shape[0]/batch_size
    

    # Let's compile a set of theano functions to do random things
    train_model = theano.function(inputs=[index], outputs=cost,
                                  updates=updates,
                                  givens={x: train_shared_set[index * batch_size:(index + 1) * batch_size]})
    
    # Same as train_model but on validation set and no updates
    test_model = theano.function(inputs=[index], outputs=cost,
                                 givens={x: valid_shared_set[index * batch_size:(index + 1) * batch_size]})

    print 'Starting training ...'

    epoch = 0
    done_looping = False

    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1

        if epoch % 5 == 0:
            learning_rate = learning_rate/100.0

        for minibatch_index in xrange(n_train_batches):
            
            minibatch_avg_cost = train_model(minibatch_index)
            #iteration number: Actually no idea how this is calculated -P
            iter = (epoch-1) * n_train_batches + minibatch_index 

            #Will add validation code here if necessary -P
            
            #test trained model on dataset
            test_rmse = [test_model(i) for i
                         in xrange(n_train_batches)]
            test_score = numpy.mean(test_rmse)
            
            print((' epoch %i, minibatch %i/%i, error %f') %
                  (epoch, minibatch_index + 1, n_train_batches, test_score))

    # Write out encoding and decoding weights and biases
    f1 = file(enc_wts,'wb')
    for i in xrange(len(da_layers)):
        cPickle.dump(our_mlp.layers[i], f1, protocol=cPickle.HIGHEST_PROTOCOL)
    f1.close()

    f2 = file(dec_wts, 'wb')
    for i in xrange(len(da_layers), our_mlp.num_layers):
        cPickle.dump(our_mlp.layers[i], f2, protocol=cPickle.HIGHEST_PROTOCOL)
    f2.close()
    



if __name__ == '__main__':
    if len(sys.argv) <= 4:
        print "Usage: python finetune.py SDA.pkl_model INPUTDATA.ascii ENC.wts DEC.wts"
        exit(0)
    else:
        sda_model = sys.argv[1]
        train_mlp(sda=sda_model, dataset=sys.argv[2],
                  enc_wts=sys.argv[3], dec_wts=sys.argv[4])
