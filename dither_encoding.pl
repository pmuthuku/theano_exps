#!/bin/perl

# Adds a tiny bit of noise 0.1*SD to the file so that the delta computation
# doesn't produce any zeros

use warnings;
use strict;

if($#ARGV < 0){
    print STDERR "Usage: perl dither_encoding.pl INPUT_FILE.ASCII MEAN_SD_FILE (output of mean_sd.pl)\n\n";
    exit;
}

open(Inp,"<$ARGV[0]") || die ("Input file not found");
open(MeSD,"<$ARGV[1]") || die ("Mean and SD file not found");

my (@means, @sd);
my @lin1;
my $i;
my $rand_no;

my @dat=<MeSD>;
chomp($dat[0]);
chomp($dat[1]);
@means = split(/\s+/,$dat[0]);
@sd = split(/\s+/,$dat[1]);
my $vect_len = @means;
close(MeSD);

while(<Inp>){

    chomp;
    @lin1 = split(/\s+/,$_);
    
    for($i=0; $i< $vect_len; $i++){
	$rand_no = (rand(1) - 0.5)*2; #Noise with mean 0 and SD 1
	$rand_no = $rand_no * (0.5 * $sd[$i]);
	$lin1[$i] = $lin1[$i] + $rand_no;
	print "$lin1[$i]\t";
    }

    print "\n";
}
close(Inp);
