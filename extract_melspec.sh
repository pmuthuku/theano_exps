#!/bin/bash

# This code extracts the Mel Spectrum from Mel Cepstra in a voice directory
# very stupid way because SPTK lacks the code to extract the Mel Spectrum
# directly without going through the Cepstral representation (as of version
# 3.7). 

# A lot of the parameter values I am using for the following script will
# appear to be counter intuitive. So, unless you *really* know what you are
# doing, do not change these values. -Prasanna

PROMPTFILE="$1"

if [ "$PROMPTFILE" = "" ]
then
    PROMPTFILE=etc/txt.done.data
fi


SPTK=/home/pmuthuku/toolkits/SPTK-3.7/binaries/
CODE_DIR=/home/pmuthuku/code/Theano_exps/

if [ ! -d mel_spec ]
then
    mkdir mel_spec
fi

CG_TMP=jnk_$$

cat $PROMPTFILE |
awk '{print $2}' |
while read i
do

    fname=$i
    echo "Extracting Mel spectrum for $fname"
    
    $ESTDIR/bin/ch_track mcep_deltas/$fname.mcep -c `perl -e 'for($i=0; $i < 24; $i++){ print "$i,";}print "24";'` -otype ascii | $SPTK/bin/x2x +af | $SPTK/bin/c2sp -m 24 -l 512 -o 2 | $SPTK/bin/x2x +fa257 > mel_spec/$fname.mspec

done




# Get the training and testing data for the Neural network
# Not sure if this code should be part of this script or in a script of
# its own

if [ ! -f $PROMPTFILE.train ]
then
    echo "Did not find $PROMPTFILE.train"
    exit
fi

cat $PROMPTFILE.train |
awk '{print $2}' |
while read i
do
    cat mel_spec/$i.mspec >> $CG_TMP.trn_mspec
done

mv $CG_TMP.trn_mspec mel_spec/traindata.mspec



if [ ! -f $PROMPTFILE.test ]
then
    echo "Did not find $PROMPTFILE.test"
    exit
fi

cat $PROMPTFILE.test |
awk '{print $2}' |
while read i
do
    cat mel_spec/$i.mspec >> $CG_TMP.test_mspec
done

mv $CG_TMP.test_mspec mel_spec/testdata.mspec


# Let's normalize the Mel Spectra so that the numbers have a mean of 0.5
# and a standard deviation of 0.25. Neural networks try really really hard
# to make sure that they only work with numbers from 0 to 1. For our 
# neural networks, we will be using the sigmoid as the non-linear squashing
# function. So, normalizing the numbers to have a mean of 0.5 and an SD of
# 0.25 will mean that 90% of the numbers will fall between 0 and 1 (2 SDs
# of the mean). Let's hope that this works. 

#perl /home/pmuthuku/code/error_metrics/mean_sd.pl mel_spec/traindata.mspec 1 257 > etc/traindata_meansd
#perl /home/pmuthuku/code/Theano_exps/normalize.pl mel_spec/traindata.mspec etc/traindata_meansd 1 257 > mel_spec/traindata.mspec_norm


# Let's extract the Mel Log Spectrum from the Mel spectrum
python $CODE_DIR/log_normalize.py mel_spec/traindata.mspec mel_spec/traindata.mlogspec
python $CODE_DIR/log_normalize.py mel_spec/testdata.mspec mel_spec/testdata.mlogspec



rm -f $CG_TMP.*

