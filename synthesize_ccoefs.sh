#!/bin/bash

CG_TMP=jnk_$$

for i in *.mcep
do

    fname=`basename $i .mcep`
    echo "Synthesizing $fname"
    $ESTDIR/bin/ch_track $i -c 0 -otype ascii -o $CG_TMP.f0
    $ESTDIR/bin/ch_track $fname.dec_mcep -otype ascii -o $CG_TMP.mcep

    paste $CG_TMP.f0 $CG_TMP.mcep |  $ESTDIR/bin/ch_track -itype ascii -s 0.005 -otype est_ascii -o $CG_TMP.f0mcep

    $ESTDIR/../festival/bin/festival -b '(begin (set! data (track.load "'$CG_TMP.f0mcep'"))(set! wv (mlsa_resynthesis data nil nil))(wave.save wv "'$fname.wav'" "'riff'"))'
    
done

rm -f $CG_TMP.*