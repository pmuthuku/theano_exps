#!/bin/perl

use strict;
use warnings;

my ($len, $i);
my @data;

while(<STDIN>){

    chomp;
    
    @data = split(/\s+/,$_);
    my $len=@data;
    
    for( $i=0; $i < $len/2; $i++){
	print "$data[$i]\t";
    }

    for ( $i=$len/2; $i <= $len; $i++){
	if($data[$i] < 0.0001){
	    $data[$i] = 0.0001;
	}
	print "$data[$i]\t";
    }

    print "\n";

}
