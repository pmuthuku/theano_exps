#!/bin/bash

PROMPTFILE="$1"

CODE_DIR=/home/pmuthuku/code/Theano_exps/

if [ "$PROMPTFILE" = "" ]
then
    PROMPTFILE=etc/txt.done.data
fi


if [ ! -d encd ]
then
    mkdir encd
fi


CG_TMP=jnk_$$
ENCD_NETWORK=etc/slt_linlayer.enc

cat $PROMPTFILE |
awk '{print $2}' |
while read i
do

    fname=$i
    echo "Encoding the Mel Spectrum of $fname"

    #Mel log spectrum
    python $CODE_DIR/log_normalize.py mel_spec/$fname.mspec $CG_TMP.mlogspec

    python $CODE_DIR/feedforward.py $ENCD_NETWORK $CG_TMP.mlogspec encd/$fname.encd

done

rm -f $CG_TMP.*