#!/bin/bash

# Usage: ./decode_mcep.sh DIR_NAME

DIR_NAME=$1
CODE_DIR=/home/pmuthuku/code/Theano_exps/
SPTK=/home/pmuthuku/toolkits/SPTK-3.7/binaries/

#mkdir -p ${DIR_NAME}_dec

CG_TMP=jnk_$$
DEC_NETWORK=etc/linlayer.dec

for i in test/$DIR_NAME/*.mcep
do

    fname=`basename $i .mcep`
    echo $fname

    #Get F0 and voicing out
    $ESTDIR/bin/ch_track $i -c 0 -otype ascii -o $CG_TMP.f0
    $ESTDIR/bin/ch_track $i -c 51 -otype ascii -o $CG_TMP.v

    #Get the encoding out
    $ESTDIR/bin/ch_track $i -c `perl -e 'for($i=1; $i < 50; $i++){ print "$i,";}print "50 ";'` -otype ascii -o $CG_TMP.enc

    #Decode to get Mel Spectrum out
    python $CODE_DIR/feedforward.py $DEC_NETWORK $CG_TMP.enc $CG_TMP.mspec_norm
    python $CODE_DIR/log_unnormalize.py $CG_TMP.mspec_norm $CG_TMP.mspec

    #Convert Mel Spec to Mel Cep
    $SPTK/bin/x2x +af $CG_TMP.mspec | $SPTK/bin/gcep -m 24 -l 512 -q 3 | $SPTK/bin/x2x +fa25 | $ESTDIR/bin/ch_track -itype ascii -s 0.005 -otype est_ascii > test/$DIR_NAME/$fname.dec_mcep

done

rm -f $CG_TMP.*