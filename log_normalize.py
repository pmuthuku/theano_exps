import sys
import numpy as np


if len(sys.argv) <= 1:
    print "Usage: python log_normalize.py INPUT.ascii OUTPUT.ascii"
    sys.exit(0)


op=open(sys.argv[2],'w')

with open(sys.argv[1],'r') as f:
    for line in f:
        data = np.fromstring(line, sep='\t')
        logdata = np.log(data)
        datastr = map(str,logdata)
        datastr1 = '\t'.join(datastr)
        datastr1 = datastr1 + '\n'
        op.write(datastr1)

op.close()
        
    
