#!/bin/perl

# This code reverses the transformation that normalize.pl does. Most of the
# code between the two is shared.


use warnings;
use strict;

if($#ARGV < 0){
    print STDERR "Usage: \n1:Input file \n2:File containing mean and SD (output file of mean_sd.pl\n3:Starting column number\n4:Ending column number\n\n";
    exit;
}

open(Inp,"<$ARGV[0]") || die ("Input file not found");
open(MeSD,"<$ARGV[1]") || die ("Mean and SD file not found");
my $x1=$ARGV[2];
my $x2=$ARGV[3];
my $range=$x2-$x1+1;
my @means;
my @sd;
my @zscore=( (0) x $range);
my @lin1;
my $i=0;
my $j=0;
my $k=0;

if($range < 0){
    print STDERR "Ending column number < Starting column number :( \n";
    exit;
}


#Reading in Mean
my @dat=<MeSD>;
chomp($dat[0]);
chomp($dat[1]);
@means=split(/\s+/,$dat[0]);
@sd=split(/\s+/,$dat[1]);
close(MeSD);

while(<Inp>){
    chomp;
    @lin1=split(/\s+/,$_);
    for($i=($x1-1),$j=0; $i < $x2; $i++,$j++){
	$zscore[$j] = $lin1[$i] - 0.5;
	$zscore[$j] = $zscore[$j]*4;
	$zscore[$j]=($zscore[$j]*$sd[$i])+$means[$i];
	print "$zscore[$j] ";
    }
    print "\n";
}
