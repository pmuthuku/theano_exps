#!/bin/bash

# This code tests the effectiveness of the encoding and decoding weights
# by encoding and recoding the mel spectrum and computing Mel Cepstral 
# Distance between the two set of Mel Spectra

# Usage: ./enc_dec_test.sh MEL_SPEC1 ENC_WTS DEC_WTS MEAN_MSPEC
# MEL_SPEC1 is ASCII 
# ENC_WTS and DEC_WTS are in Python shelve format
# MEAN_MSPEC is ASCII


MEL_SPEC1=$1

ENC_WTS=$2
DEC_WTS=$3
NORM_FILE=$4

CG_TMP=jnk_$$

CODE_DIR=/home/pmuthuku/code/Theano_exps/
SPTK=/home/pmuthuku/toolkits/SPTK-3.7/binaries/

# Let's encode the Mel Spectrum into our low(?)-dimensional space
python $CODE_DIR/feedforward.py $ENC_WTS $MEL_SPEC1 $CG_TMP.encod

# Decoding our Mel Spectrum
python $CODE_DIR/feedforward.py $DEC_WTS $CG_TMP.encod $CG_TMP.mspec_decod

# Unnormalizing data
#perl $CODE_DIR/unnormalize.pl $CG_TMP.mspec_decod $NORM_FILE 1 257 > $CG_TMP.mspec_decod_unnorm
#perl $CODE_DIR/unnormalize.pl $MEL_SPEC1 $NORM_FILE 1 257 > $CG_TMP.mspec_unnorm
python $CODE_DIR/log_unnormalize.py $CG_TMP.mspec_decod $CG_TMP.mspec_decod_unnorm
python $CODE_DIR/log_unnormalize.py $MEL_SPEC1 $CG_TMP.mspec_unnorm

# Mel Spectrum is always positive so removing negative numbers
#python $CODE_DIR/floor_zeros.py $CG_TMP.mspec_decod_unnorm $CG_TMP.mspec_decod_unnorm_floored

# Reference MCEPs
$SPTK/bin/x2x +af $CG_TMP.mspec_decod_unnorm | $SPTK/bin/gcep -m 24 -l 512 -q 3 | $SPTK/bin/x2x +fa25 | $ESTDIR/bin/ch_track -itype ascii -s 0.005 -otype est_ascii > $CG_TMP.mcepa
# Decoded MCEPs
$SPTK/bin/x2x +af $CG_TMP.mspec_unnorm | $SPTK/bin/gcep -m 24 -l 512 -q 3 | $SPTK/bin/x2x +fa25 | $ESTDIR/bin/ch_track -itype ascii -s 0.005 -otype est_ascii > $CG_TMP.mcepb

$CODE_DIR/track_diff_mcd $CG_TMP.mcepa $CG_TMP.mcepb

#x=`perl /home/pmuthuku/code/error_metrics/rms_error.pl $CG_TMP.mcepa $CG_TMP.mcepb 1 25`
#echo "$x*6.141" | bc

rm -f $CG_TMP.*