#!/bin/bash

# Usage: ./compute_voice_mcd.sh DIR_NAME

DIR_NAME=$1

CODE_DIR=/home/pmuthuku/code/Theano_exps/
SPTK=/home/pmuthuku/toolkits/SPTK-3.7/binaries/

CG_TMP=jnk_$$

for i in test/$DIR_NAME/*.dec_mcep
do

    fname=`basename $i .dec_mcep`
    
    $ESTDIR/bin/ch_track mcep_deltas/$fname.mcep -c `perl -e 'for($i=0; $i < 24; $i++){ print "$i,";}print "24 ";'` -otype ascii | $SPTK/bin/x2x +ad > $CG_TMP.mcep_true

    $ESTDIR/bin/ch_track $i -otype ascii | $SPTK/bin/x2x +ad > $CG_TMP.mcep_synth


    $FESTVOXDIR/src/vc/src/dtw/dtw -nmsg -dim 25 -sdim 0 -ldim 24 -getcd $CG_TMP.mcep_true $CG_TMP.mcep_synth
    

done

rm -f $CG_TMP.*