import sys
import numpy as np


if len(sys.argv) <= 1:
    print "Usage: python floor_zeros.py INPUT.ascii OUTPUT.ascii"
    sys.exit(0)


ipdata = np.loadtxt(sys.argv[1], dtype="float32")

ipdata[ipdata < 0] = 0.001

#np.clip(ipdata,0,np.max(ipdata),out=ipdata)

np.savetxt(sys.argv[2], ipdata, delimiter='\t', newline='\n')


