#!/bin/bash

# This script tests if the extracted Mel spectrum is correct by converting
# the Mel spectrum back to the Mel cepstrum and computing the RMSE between
# this Mel Cepstrum and the original Mel Cepstrum. Ideally, this number 
# should be zero or at least very small 


PROMPTFILE="$1"

if [ "$PROMPTFILE" = "" ]
then
    PROMPTFILE=etc/txt.done.data
fi


SPTK=/home/pmuthuku/toolkits/SPTK-3.7/binaries/

CG_TMP=jnk_$$

cat $PROMPTFILE |
awk '{print $2}' |
while read i
do

    fname=$i
    
    #Reference
    $ESTDIR/bin/ch_track mcep_deltas/$fname.mcep -c `perl -e 'for($i=0; $i < 24; $i++){ print "$i,";}print "24";'` -otype ascii -o $CG_TMP.mcepa

    #MCEPs from Mel Spectra
    $SPTK/bin/x2x +af mel_spec/$fname.mspec | $SPTK/bin/gcep -m 24 -l 512 -q 3 | $SPTK/bin/x2x +fa25 > $CG_TMP.mcepb

    perl /home/pmuthuku/code/error_metrics/rms_error.pl $CG_TMP.mcepa $CG_TMP.mcepb 1 25 >> $CG_TMP.in_scores

done 

perl /home/pmuthuku/code/error_metrics/mean_sd.pl $CG_TMP.in_scores 1 1 | head -n 2

rm -f $CG_TMP.*




