#!/bin/bash

if [ ! -d ccoefs ]
then
    mkdir ccoefs
fi
CG_TMP=cg_tmp_$$

if [ "$PROMPTFILE" = "" ]
then
   if [ $# = 1 ]
   then
      PROMPTFILE=$1
   else
      PROMPTFILE=etc/txt.done.data
   fi
fi


cat $PROMPTFILE |
awk '{print $2}' |
while read i
do
    fname=$i
    echo $fname "COMBINE_COEFFS (f0, DNN Encoding, DNN deltas, v)"
    
    # F0
    $ESTDIR/bin/ch_track -otype ascii f0/$fname.f0 |
    awk '{if (NR == 1) { print $1; print $1} print $1}' >$CG_TMP.f0

    # DNN Encoding
    cat encd/$fname.encd_delt > $CG_TMP.encd

    if [ -f v/$fname.v ]
    then
	cat v/$fname.v | awk '{print 10*$1}' > $CG_TMP.v
	paste $CG_TMP.f0 $CG_TMP.encd $CG_TMP.v
    else
	paste $CG_TMP.f0 $CG_TMP.encd
    fi |
    awk '{if (l==0) 
              l=NF;
            else if (l == NF)
              print $0}' |
    awk '{if (NR == -1) print $0; print $0}' |
    $ESTDIR/bin/ch_track -itype ascii -otype est_binary -s 0.005 -o ccoefs/$fname.mcep

    rm -f $CG_TMP.*
done

exit